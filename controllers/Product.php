<?php

namespace controllers;

use core\Controller;
use core\Core;
use models\CategoryModel;
use models\ProductModel;

class Product extends Controller
{
    protected $user;
    protected $newsModel;
    protected $userModel;

    public function __construct()
    {
        $this->userModel = new \models\Users();
        $this->productModel = new \models\ProductModel();
        $this->user = $this->userModel->GetCurrentUser();

    }

    public function actionIndex()
    {
        $products = $this->productModel->getAllProducts();
        return $this->render('index', [
            'products' => $products
        ]);
    }

    public function actionAdd()
    {
        if (!empty($_GET['id'])) {
            $category_id = $_GET['id'];
            echo 'work';
        } else
            $category_id = null;
        $categories = CategoryModel::GetCategories();
        $titleForbidden = 'Доступ заборонено';
        if (empty($this->user))
            return $this->render('forbidden', null, [
                'PageTitle' => $titleForbidden,
                'MainTitle' => $titleForbidden
            ]);
        $title = 'Додавання товару';
        if ($this->isPost()) {
            $result = $this->productModel->addProducts($_POST);
            if ($result['error'] === false) {
                return $this->renderMessage('ok', 'Товар успішно додано', null, [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                ]);
            } else {
                $message = implode('<br/>', $result['messages']);
                return $this->render('add',
                    [
                        'name' => $_POST,
                        'category_id' => $category_id,
                        'categories' => $categories,
                    ],
                    [
                        'PageTitle' => $title,
                        'MainTitle' => $title,
                        'MessageText' => $message,
                        'MessageClass' => 'danger',
                    ]);
            }
        } else {
            $params =
                [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                ];
            return $this->render('add',
                [
                    'name' => $_POST,
                    'categories' => $categories,
                    'category_id' => $category_id
                ], $params);
        }
    }

    public function actionView()
    {
        $id = $_GET['id'];
        $product = ProductModel::getProductById($id);
        return $this->render('view', [
            'product' => $product
        ]);
    }

    public function actionEdit()
    {
        if (!empty($_GET['id'])) {
            $category_id = $_GET['id'];
        } else
            $category_id = null;
        $categories = CategoryModel::GetCategories();
        $id = $_GET['id'];
        $title = 'Редагування вмісту товару';
        if ($this->isPost()) {
            $result = ProductModel::updateProduct($id, $_POST);
            if ($result === true) {
                return $this->renderMessage('ok', 'Товар успішно збережено', null, [
                    'PageTitle' => $title,
                ]);
            } else {
                return $this->render('add',
                    [
                        'categories' => $categories,
                        'category_id' => $category_id
                    ],
                    [
                        'PageTitle' => $title,
                        'MessageClass' => 'danger',
                    ]);
            }
        } else {
            $params =
                [
                    'PageTitle' => $title,
                ];
            return $this->render('add', [
                'name' => $_POST,
                'categories' => $categories,
                'category_id' => $category_id
            ], $params);
        }
    }

    public function actionDelete()
    {
        $title = 'Видалення товару';
        $id = $_GET['id'];
        if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {
            if (ProductModel::deleteProduct($id))
                return $this->redirect('/product/');
            else
                return $this->renderMessage('error', 'Помилка видалення товару', null, [
                    'PageTitle' => $title,
                    'MainTitle' => $title,

                ]);
        }
        $news = ProductModel::getProductById($id);
        return $this->render('delete', ['model' => $news], [
            'PageTitle' => $title,
            'MainTitle' => $title
        ]);
    }

}