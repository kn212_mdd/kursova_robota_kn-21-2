<?php

namespace models;

class BasketModel
{
    public static function addProduct($product_id, $count = 1)
    {
        if (!is_array($_SESSION['basket']))
            $_SESSION['basket'] = [];
        $_SESSION['basket'][$product_id] += $count;
    }

    public static function GetProductsInBasket()
    {
        if (!empty($_SESSION['basket']) && is_array($_SESSION['basket'])) {
            $result = [];
            $products = [];
            $totalPrice = 0;
            foreach ($_SESSION['basket'] as $product_id => $count) {
                $product = ProductModel::getProductById($product_id);
                $totalPrice += $product[0]['price'] * $count;
                $products [] = ['product' => $product[0], 'count' => $count];
            }
            $result['products'] = $products;
            $result['total_price'] = $totalPrice;
            return $result;
        }
        return null;
    }
}

