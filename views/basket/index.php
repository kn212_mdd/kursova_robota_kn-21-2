<?php
/** @var array $basket */

?>
<section class="h-80 h-custom">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-12">
                <div class="card card-registration card-registration-2" style="border-radius: 15px;">
                    <div class="card-body p-0">
                        <div class="row g-0">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="d-flex justify-content-between align-items-center mb-5">
                                        <h1 class="fw-bold mb-0 text-black">Кошик</h1>
                                    </div>
                                    <hr class="my-4">
                                    <?php
                                    $index = 1;
                                    if (!empty($basket['products']))
                                        foreach ($basket['products'] as $row) : ?>
                                            <div class="row mb-4 d-flex justify-content-between align-items-center">
                                                <div class="col-md-3 col-lg-3 col-xl-3">
                                                    <h6 class="text-muted"><?= $row['product']['name'] ?></h6>
                                                </div>
                                                <div class="col-md-3 col-lg-3 col-xl-2 d-flex">
                                                    <td><input type="number" class="form-control"
                                                               value="<?= $_SESSION['countbought'] ?>" min="1" readonly
                                                               max="<?= $row['product']['count'] ?>" шт.</td>
                                                </div>
                                                <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                                                    <h6 class="mb-0">
                                                        <td id="pricePerOne"><?= $row['product']['price'] ?> грн.</td>
                                                    </h6>
                                                </div>
                                            </div>
                                            <?php
                                            $index++;
                                        endforeach;
                                    ?>
                                    <hr class="my-4">
                                    <div class="d-flex justify-content-between mb-1">
                                        <h5 class="text-uppercase">Сумарно:</h5>
                                        <th><?= $basket['total_price'] ?> грн.</th>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form method="post" action="">
            <input type="submit" class="btn btn-danger mt-2" value="Очистити кошик" name="clear" id="clear">
        </form>
        <form method="post" action="">
            <input type="submit" class="btn btn-success mt-2" value="Придбати" name="buy" id="buy">
        </form>

        <?php
        if (array_key_exists('clear', $_POST))
            unset($_SESSION['basket']);
        ?>
        <script>
            let $priceperone = document.getElementById('pricePerOne');
            console.log($priceperone);
        </script>

        <script>
            document.getElementById('buy').addEventListener('click', function (event) {
                event.preventDefault(); // Зупинити стандартну поведінку форми

                <?php if(!empty($_SESSION['basket'])) : ?>
                alert('Товар придбано!')
                <?php unset($_SESSION['basket']); ?>
                <?php else: ?>
                alert('Кошик пустий')
                <?php endif; ?>
            });
        </script>








