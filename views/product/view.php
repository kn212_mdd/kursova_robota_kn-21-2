<?php
/** @var array $product */

$userModel = new models\Users();
?>

<h1 class="h3 mb-3 fw-normal text-center"><?= $product[0]['model'] ?></h1>

<div class="container">
    <div class="row">
        <div class="col-6">
            <?php
            $filePath = 'files/product/' . $product[0]['photo']; ?>
            <?php if (is_file($filePath)) : ?>
                <img src="/<?= $filePath ?>" class="img-thumbnail " alt="...">
            <?php else : ?>
                <img src="/image/no_image.jpg" class="img-thumbnail" alt="...">
            <?php endif; ?>
        </div>
        <div class="col-6">
            <div class="container">
                <div class="row mb-3">
                    <div class="col-4">
                        Ціна товару:
                    </div>
                    <div class="col-8">
                        <strong><?= $product[0]['price'] ?> грн.</strong>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-4">
                        Кількість на складі:
                    </div>
                    <div class="col-8">
                        <strong><?= $product[0]['count'] ?> шт.</strong>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-4">
                        Опис:
                    </div>
                    <div class="col-8">
                        <strong><?= $product[0]['short_text'] ?> </strong>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-4">
                        Придбати:
                    </div>
                    <?php
                    if($userModel->IsUserAuthenticated()): ?>
                    <div class="col-4">
                        <form method="post" action="">
                            <input min="1" value="1" max="<?=$product[0]['count']?>" class="form-control mb-2" type="number" name="count" id="count"/>
                            <div>
                                <button type="submit" class="btn btn-success" name="addToBasket" id="addToBasket">Додати до кошика</button>
                            </div>
                        </form>
                    </div>
                    <?php else: ?>
                        <div class="col-4">
                            <p style="color: red; font-weight: bold;">Потрібно аутентифікуватися, щоб додати товар до кошика.</p>
                        </div>
                    <?php endif; ?>
                    <?php
                    $_SESSION['countbought']=$_POST['count'];
                    if (array_key_exists('addToBasket', $_POST)) {
                        \models\BasketModel::AddProduct($product[0]['id'], $_POST['count']);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>




