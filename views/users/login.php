<?php
unset($_SESSION['basket']);
?>

<section class="vh-100">
    <div class="container-fluid h-custom">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-md-9 col-lg-6 col-xl-5">
                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                     class="img-fluid" alt="Sample image">
            </div>
            <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                <form method="post" action="">
                    <div class="form-outline mb-4">
                        <label for="login">Логін</label>
                        <input type="text" name="login" class="form-control" id="login" placeholder="Логін">
                    </div>
                    <div class="form-outline mb-3">
                        <label for="password">Пароль</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Пароль">
                    </div>

                    <div class="text-center text-lg-start mt-4 pt-2">
                        <button type="submit" class="btn btn-dark btn-lg"
                                style="padding-left: 2.5rem; padding-right: 2.5rem;">Увійти
                        </button>
                        <p class="small fw-bold mt-2 pt-1 mb-0">Досі немає акаунта? <a href="/users/register" class="link-danger">Реєстрація</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

